<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/_profiler' => [[['_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'], null, null, null, true, false, null]],
        '/_profiler/search' => [[['_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'], null, null, null, false, false, null]],
        '/_profiler/search_bar' => [[['_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'], null, null, null, false, false, null]],
        '/_profiler/phpinfo' => [[['_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'], null, null, null, false, false, null]],
        '/_profiler/open' => [[['_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'], null, null, null, false, false, null]],
        '/default' => [[['_route' => 'default', '_controller' => 'App\\Controller\\DefaultController::index'], null, null, null, false, false, null]],
        '/' => [[['_route' => 'index', '_controller' => 'App\\Controller\\DefaultController::index'], null, null, null, false, false, null]],
        '/api/records/nosniki' => [
            [['_route' => 'nosniki_list', '_controller' => 'App\\Controller\\NosnikiController:indexAction'], null, ['GET' => 0], null, false, false, null],
            [['_route' => 'nosniki_delete', '_controller' => 'App\\Controller\\NosnikiController:deleteAllAction'], null, ['DELETE' => 0], null, true, false, null],
        ],
        '/api/records/nosniki/add' => [[['_route' => 'nosnik_create', '_controller' => 'App\\Controller\\NosnikiController:createAction'], null, ['POST' => 0], null, false, false, null]],
        '/api/records/utwory' => [
            [['_route' => 'utwory_list', '_controller' => 'App\\Controller\\UtworyController:indexAction'], null, ['GET' => 0], null, false, false, null],
            [['_route' => 'utwor_create', '_controller' => 'App\\Controller\\UtworyController:createAction'], null, ['POST' => 0], null, false, false, null],
        ],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/_(?'
                    .'|error/(\\d+)(?:\\.([^/]++))?(*:38)'
                    .'|wdt/([^/]++)(*:57)'
                    .'|profiler/([^/]++)(?'
                        .'|/(?'
                            .'|search/results(*:102)'
                            .'|router(*:116)'
                            .'|exception(?'
                                .'|(*:136)'
                                .'|\\.css(*:149)'
                            .')'
                        .')'
                        .'|(*:159)'
                    .')'
                .')'
                .'|/api/records/(?'
                    .'|nosniki/([^/]++)(?'
                        .'|(*:204)'
                        .'|/utwory(?'
                            .'|(*:222)'
                        .')'
                    .')'
                    .'|utwory/([^/]++)(?'
                        .'|(*:250)'
                    .')'
                .')'
            .')/?$}sD',
    ],
    [ // $dynamicRoutes
        38 => [[['_route' => '_preview_error', '_controller' => 'error_controller::preview', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
        57 => [[['_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'], ['token'], null, null, false, true, null]],
        102 => [[['_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'], ['token'], null, null, false, false, null]],
        116 => [[['_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'], ['token'], null, null, false, false, null]],
        136 => [[['_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception_panel::body'], ['token'], null, null, false, false, null]],
        149 => [[['_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception_panel::stylesheet'], ['token'], null, null, false, false, null]],
        159 => [[['_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'], ['token'], null, null, false, true, null]],
        204 => [
            [['_route' => 'nosnik_show', '_controller' => 'App\\Controller\\NosnikiController:showAction'], ['id'], ['GET' => 0], null, false, true, null],
            [['_route' => 'nosnik_update', '_controller' => 'App\\Controller\\NosnikiController:updateAction'], ['id'], ['POST' => 0, 'PUT' => 1, 'PATCH' => 2], null, false, true, null],
            [['_route' => 'nosnik_delete', '_controller' => 'App\\Controller\\NosnikiController:deleteAction'], ['id'], ['DELETE' => 0], null, false, true, null],
        ],
        222 => [
            [['_route' => 'utwory_nosnika', '_controller' => 'App\\Controller\\UtworyController:listAction'], ['id'], ['GET' => 0], null, false, false, null],
            [['_route' => 'utwory_delete', '_controller' => 'App\\Controller\\UtworyController:deleteAllAction'], ['id'], ['DELETE' => 0], null, false, false, null],
        ],
        250 => [
            [['_route' => 'utwor_show', '_controller' => 'App\\Controller\\UtworyController:showAction'], ['id'], ['GET' => 0], null, false, true, null],
            [['_route' => 'utwor_update', '_controller' => 'App\\Controller\\UtworyController:updateAction'], ['id'], ['OPTIONS' => 0, 'POST' => 1, 'GET' => 2], null, false, true, null],
            [['_route' => 'utwor_delete', '_controller' => 'App\\Controller\\UtworyController:deleteAction'], ['id'], ['DELETE' => 0], null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
