<?php

namespace App\DataFixtures;

use App\Entity\Nosniki;
use App\Entity\Utwory;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function __construct() { }

    public function load(ObjectManager $manager)
    {
          for ($i = 1; $i < 20; $i++)
          {
              $nosnik = new Nosniki();

              $nosnik->setArtysta('Famous Artist '.$i);
              $nosnik->setTytul('Album '.$i);
              $nosnik->setRok(rand(1971, 2021));
              switch(rand(1,3))
              {
                case 1:  $ntype='Vinyl'; break;
                case 2:  $ntype='CD'; break;
                case 3:  $ntype='MP3'; break;
                default: $ntype='DVD'; break;
              }
              $nosnik->setNtype($ntype);
             $manager->persist($nosnik);
           }
           $manager->flush();


           $nosniki = $manager->getRepository(Nosniki::class)->findAll();
           foreach ($nosniki as $nos)
           {
             //$nosnik_id = (int) 1; //$nos->getId();
             for ($j = 1; $j < 10; $j++)
             {
                 $utwor = new Utwory();
                 $utwor->setTytul('Song '.$j);
                 $utwor->setNosniki($nos);
                $manager->persist($utwor);
             }
           }
          $manager->flush();
    }
}
