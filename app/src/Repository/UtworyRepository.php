<?php

namespace App\Repository;

use App\Entity\Utwory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Utwory|null find($id, $lockMode = null, $lockVersion = null)
 * @method Utwory|null findOneBy(array $criteria, array $orderBy = null)
 * @method Utwory[]    findAll()
 * @method Utwory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UtworyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Utwory::class);
    }

    // /**
    //  * @return Utwory[] Returns an array of Utwory objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Utwory
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
