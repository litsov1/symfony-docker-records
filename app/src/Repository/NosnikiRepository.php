<?php

namespace App\Repository;

use App\Entity\Nosniki;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Nosniki|null find($id, $lockMode = null, $lockVersion = null)
 * @method Nosniki|null findOneBy(array $criteria, array $orderBy = null)
 * @method Nosniki[]    findAll()
 * @method Nosniki[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NosnikiRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Nosniki::class);
    }

    // /**
    //  * @return Nosniki[] Returns an array of Nosniki objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Nosniki
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
