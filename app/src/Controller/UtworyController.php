<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Utwory;
use App\Form\UtworyType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;


class UtworyController extends AbstractApiController
{
    public function indexAction()
    {
        $utwory = $this->getDoctrine()->getRepository(Utwory::class)->findAll();
        return $this->json($utwory);
    }

    public function listAction(Request $request): Response
    {
        $nosnikId = $request->get('id');
        $utwory = $this->getDoctrine()->getRepository(Utwory::class)->findBy(['nosniki' => $nosnikId] );
        return $this->json($utwory);
    }

    public function showAction(Request $request): Response
    {
        $utworId = $request->get('id');
        $utwor = $this->getDoctrine()->getRepository(Utwory::class)->findOneBy(['id'=> $utworId]);
      return $this->json($utwor);
    }

    public function createAction(Request $request): Response
    {
       $form = $this->buildForm(UtworyType::class);
       $form->handleRequest($request);
          if(!$form->isSubmitted() || !$form->isValid()  ) { print(" my Utwory form error ");  }
      /** @var Utwory $utwory*/
        $utwory = $form->getData();

        $this->getDoctrine()->getManager()->persist($utwory);
        $this->getDoctrine()->getManager()->flush();

      return $this->json($utwory);
    }

    public function updateAction(Request $request): Response
    {
          $utworId = $request->get('id');
          $utwor = $this->getDoctrine()->getRepository(Utwory::class)->findOneBy(['id'=> $utworId]);

          $form = $this->buildForm(UtworyType::class);
          $form->handleRequest($request);
          $formutwory = $form->getData();

          $utwor->setTytul($formutwory->getTytul());
          $this->getDoctrine()->getManager()->flush();

        return $this->json($utwor);
    }

    public function deleteAction(Request $request): Response
    {
          $utworId = $request->get('id');
          $utwor = $this->getDoctrine()->getRepository(Utwory::class)->findOneBy(['id'=> $utworId]);

          $this->getDoctrine()->getManager()->remove($utwor);
          $this->getDoctrine()->getManager()->flush();

        return $this->json('{}');
    }

    public function deleteAllAction(Request $request): Response
    {
          $nosnikId = $request->get('id');
          $utwory = $this->getDoctrine()->getRepository(Utwory::class)->findBy(['nosniki' => $nosnikId] );
          foreach ($utwory as $utwor)       //print gettype($utwory);
          {
            $this->getDoctrine()->getManager()->remove($utwor);
          }
          $this->getDoctrine()->getManager()->flush();

        return $this->json('{}');
    }
}
