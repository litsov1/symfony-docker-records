<?php

declare(strict_types=1);

namespace App\Controller;
//namespace AppBundle\Controller;

use App\Entity\Nosniki;
use App\Form\NosnikiType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
//use Symfony\Component\Routing\Annotation\Route;
//use Doctrine\ORM\EntityManagerInterface;



class NosnikiController extends AbstractApiController
{
    // /**
    //  * @Route("/nosniki", name="nosniki")
    //  */
    public function indexAction()
    {
        $nosniki = $this->getDoctrine()->getRepository(Nosniki::class)->findAll(); // persistentObject:
        return $this->json($nosniki);
    }





    public function createAction(Request $request): Response
    {
       $form = $this->buildForm(NosnikiType::class);
       $form->handleRequest($request); //var_dump(get_object_vars($form));

           if(!$form->isSubmitted() || !$form->isValid()  ) { print(" my form error "); exit; } //

      /** @var Nosniki $nosniki*/
      $nosniki = $form->getData();

      $this->getDoctrine()->getManager()->persist($nosniki);
      $this->getDoctrine()->getManager()->flush();

      return $this->json($nosniki); //json($form);
    }




    public function showAction(Request $request): Response
    {
        $nosnikId = $request->get('id');
            if(!$nosnikId) { throw new NotFoundHttpException(); }

        $nosnik = $this->getDoctrine()->getRepository(Nosniki::class)->findOneBy(['id'=> $nosnikId]);
      return $this->json($nosnik);
    }



    public function updateAction(Request $request): Response
    {
        $nosnikId = $request->get('id');
        $nosnik = $this->getDoctrine()->getRepository(Nosniki::class)->findOneBy(['id'=> $nosnikId]);
          if(!$nosnik) { throw new NotFoundHttpException('ne ma takego Nosnika'); }

        $form = $this->buildForm(NosnikiType::class);
        $form->handleRequest($request);
             if(!$form->isSubmitted() || !$form->isValid()  ) { print(" my form error "); exit; } //
        $formnosnik = $form->getData();

        $nosnik->setArtysta($formnosnik->getArtysta());
        $nosnik->setTytul($formnosnik->getTytul());
        $nosnik->setNtype($formnosnik->getNtype());
        $nosnik->setRok($formnosnik->getRok());
        $this->getDoctrine()->getManager()->flush();

      return $this->json($nosnik);
    }

    public function deleteAction(Request $request): Response
    {
        $nosnikId = $request->get('id');
        $nosnik = $this->getDoctrine()->getRepository(Nosniki::class)->findOneBy(['id'=> $nosnikId]);

        $this->getDoctrine()->getManager()->remove($nosnik);
        $this->getDoctrine()->getManager()->flush();

      return $this->json('{}');
    }

    public function deleteAllAction(Request $request): Response
    {
          $nosniki = $this->getDoctrine()->getRepository(Nosniki::class)->findAll();
          foreach ($nosniki as $nosnik)       //print gettype($utwory);
          {
            $this->getDoctrine()->getManager()->remove($nosnik);
          }
          $this->getDoctrine()->getManager()->flush();

        return $this->json('{}');
    }

}
