<?php

namespace App\Controller;

use App\Entity\Nosniki;
//use App\Controller\FormInterface;

// use Symfony\Component\Form\AbstractType;
// use Symfony\Component\Form\FormBuilderInterface;
// use Symfony\Component\Form\Extension\Core\Type\SubmitType;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractApiController extends AbstractController
{
    protected function buildForm(string $type, $data=null, array $options =[]) // : FormInterface
    {
      $options = array_merge($options, [
        'csrf_protection' => false
        ]);

      return $this->container->get('form.factory')->createNamed('', $type, $data, $options);
    }
}
