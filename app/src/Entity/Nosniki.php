<?php

namespace App\Entity;

use App\Repository\NosnikiRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @ORM\Entity(repositoryClass=NosnikiRepository::class)
 */
class Nosniki
{
    /**
    * @ORM\OneToMany(targetEntity="Utwory", mappedBy="nosniki")
    */
    protected $utwory;

    public function __construct()
    {
            $this->utwory = new ArrayCollection();
    }

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;



    /**
     * @ORM\Column(type="string", nullable=false, length=255)
     */
    private $artysta;

    /**
     * @ORM\Column(type="string", nullable=false, length=255)
     */
    private $tytul;


    /**
     * @ORM\Column(type="string", nullable=false, length=10)
     */
    private $ntype;


    /**
     * @ORM\Column(type="string", length=10)
     */
    private $rok;



        public function getId(): ?int
        {
            return $this->id;
        }

        /**
       * @return mixed
       */
        public function getArtysta()
        {
            return $this->artysta;
        }

      /**
       * @param mixed $artysta
       */
        public function setArtysta($artysta): void
        {
            $this->artysta = $artysta;
        }

      /**
     * @return mixed
     */
      public function getTytul()
      {
          return $this->tytul;
      }

    /**
     * @param mixed $tytul
     */
      public function setTytul($tytul): void
      {
          $this->tytul = $tytul;
      }

    /**
   * @return mixed
   */
    public function getNtype()
    {
        return $this->ntype;
    }

  /**
   * @param mixed $ntype
   */
    public function setNtype($ntype): void
    {
        $this->ntype = $ntype;
    }

  /**
   * @return mixed
   */
    public function getRok()
    {
        return $this->rok;
    }

  /**
   * @param $rok
   */
  public function setRok($rok): void
  {
      $this->rok = $rok;
  }

}
