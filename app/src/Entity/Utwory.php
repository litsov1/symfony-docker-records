<?php

namespace App\Entity;

use App\Repository\UtworyRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UtworyRepository::class)
 */

class Utwory
{
    /**
    * @ORM\ManyToOne(targetEntity="Nosniki", inversedBy="utwory")
    * @ORM\JoinColumn(name="nosnik_id", referencedColumnName="id", onDelete="CASCADE")
    */
      protected $nosniki;


    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\Column(type="string", length=100)
     */
    private $tytul;


    public function getId(): ?int
    {
        return $this->id;
    }



    public function getTytul(): ?string
    {
        return $this->tytul;
    }

    public function setTytul($tytul): void
    {
        $this->tytul = $tytul;
    }

    // public function getNosnikId(): int
    // {
    //     return $this->nosnik_id;
    // }
    //
    // public function setNosnikId($nosnik_id): void
    // {
    //     $this->nosnik_id = $nosnik_id;
    // }

    public function getNosniki()//: int
    {
        return $this->nosniki;
    }

    public function setNosniki($nosniki): void
    {
        $this->nosniki = $nosniki;
    }


}
