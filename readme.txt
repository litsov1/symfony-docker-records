INSTALL:
1- clone project: git pull https://gitlab.com/litsov1/symfony-docker-records.git
2- go to project folder: symfony-docker-records 
3- run: docker-compose build 
4- run: docker-compose up
5- go inside: docker exec -it symfony-records-php-cli bash
5.1 run: php bin/console make:migration
5.2 run: php bin/console doctrine:migrations:migrate
5.3 run: php bin/console doctrine:fixtures:load

TESTY:
Temporary frontend is available: http://127.0.0.1:8081 

I used static Routs to API (routes.yaml)  
Example for Postman:

method GET  for show all Nosniki:  http://127.0.0.1:8081/api/records/nosniki

method POST for Create Nosnik:  http://127.0.0.1:8081/api/records/nosniki/add
{
    "artysta": "TestArtysta",
    "tytul": "Test Title",
    "ntype": "mp3",
    "rok":  "2021"   
}
method GET for show one Nosnik:  http://127.0.0.1:8081/api/records/nosniki/{nosnik.id}

method DELETE delete all http://127.0.0.1:8081/api/records/nosniki/

method DELETE delete one Nosnik http://127.0.0.1:8081/api/records/nosniki/{nosnik.id}
